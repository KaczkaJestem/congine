#pragma once

#include <conio.h>
#include "IEventReceiver.h"

namespace Engine
{
	class CEventReceiver : public IEventReceiver
	{
		public:
			CEventReceiver();

			~CEventReceiver();

			virtual void	setUserInterface(IUserInterface* _ui);
			virtual bool	isKeyPressed(int keyCode);
			virtual void	update();
	};
}; 