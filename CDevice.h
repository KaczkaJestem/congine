#pragma once

#include "IDevice.h"
#include "CViewport.h"
#include "CUserInterface.h"

namespace Engine
{
	class CDevice : public IDevice
	{
		public:
			CDevice(Rect<unsigned int> _WindowRect, Rect<unsigned int> _ViewportRect, char uiBackgrChar = (char) 178);
			~CDevice();

			virtual void stopDevice();

			virtual IUserInterface* getUserInterface();
			virtual IViewport*		getViewport();
			virtual IEventReceiver* getEventReceiver();

			virtual int				getFPS();
			virtual void			setFPSLimit(unsigned int limit);

			virtual void			setEventReceiver(IEventReceiver* _receiver);

			virtual void			displayAll();
	};
};