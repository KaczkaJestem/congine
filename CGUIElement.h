#pragma once

#include "IUserInterface.h"
#include "IGUIElement.h"

namespace Engine
{
	class CGUIElement : public IGUIElement
	{
		protected:
			virtual bool isClipped();

		public:
			CGUIElement(Rect<unsigned int> _rectangle, int id = -1, bool pickable = false);
			~CGUIElement();

			virtual void				makeActive(bool a);

			virtual bool				isPickable();
			virtual int					getID();
			virtual bool				hasChanged();
			virtual Rect<unsigned int>	getRect();
			virtual void				drawOut(char** table);

			virtual void _register(IUserInterface* _ui);
			virtual void _unregister(IUserInterface* _ui);
	};

};