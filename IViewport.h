#pragma once

#include <vector>
#include "IUserInterface.h"
#include "Rectangle.h"
#include "IObject.h"

namespace Engine
{
	class IViewport abstract
	{
		protected:
			std::vector<IObject*> object_list;

			Rect<unsigned int> rectangle;
			unsigned int width;
			unsigned int height;

			bool listChanged;

			char** viewport;

		public:
			IViewport(Rect<unsigned int> _rectangle)
				: rectangle(_rectangle),
				  listChanged(false)
			{	
				width = _rectangle.getWidth();
				height = _rectangle.getHeight();

				viewport = new char* [height];
				 
				for(unsigned int i = 0; i < height; i++)
					viewport[i] = new char[width];
			}

			~IViewport() {}

			virtual void update() = 0;
			virtual void clearViewport() = 0;
			virtual void drawObject(IObject* obj) = 0;
			virtual void drawAll() = 0;
			virtual void addToList(IObject* obj) = 0;
			virtual void removeFromList(IObject* obj) = 0;
			virtual void display() = 0;

			virtual IObject* addObject(Geometry* geometry, int collisionLvl) = 0;

			virtual Rect<unsigned int>	getRect() = 0;
			virtual unsigned int		getHeight() = 0;
			virtual unsigned int		getWidth() = 0;

			virtual char**				getVPTable() = 0;
			virtual IObject*			getObjectByPoint(Position2d<unsigned int> point, IObject* exception = NULL) = 0;
	};

};