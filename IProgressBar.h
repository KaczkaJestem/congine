#pragma once

#include <string>
#include "IGUIElement.h"
#include "Rectangle.h"

namespace Engine
{

	class IProgressBar abstract : public IGUIElement
	{
		protected:
			std::string text;

			int minValue;
			int currValue;
			int maxValue;

			virtual bool isClipped() = 0;

		public:
			IProgressBar(Rect<unsigned int> rect, int max, int curr, int min, int _id = -1, bool _pickable = false)
				: IGUIElement(rect, _id, _pickable),
				  maxValue(max),
				  currValue(curr),
				  minValue(min)
			{}

			~IProgressBar() {}

			virtual void		makeActive(bool a) = 0;
			virtual int			getID() = 0;
			virtual bool		isPickable() = 0;
			virtual void		setMinValue(int value) = 0;
			virtual void		setMaxValue(int value) = 0;
			virtual void		setCurrValue(int value) = 0;

			virtual int					getMinValue() = 0;
			virtual int					getCurrValue() = 0;
			virtual int					getMaxValue() = 0;
			virtual Rect<unsigned int>	getRect() = 0;
			virtual void				drawOut(char** table) = 0;
	};

};