#include "CViewport.h"

namespace Engine
{
	IViewport* createViewport(Rect<unsigned int> _rectangle)
	{
		return new CViewport(_rectangle);
	}


	CViewport::CViewport(Rect<unsigned int> _rectangle)
		: IViewport(_rectangle)
	{
		clearViewport();
	}

	CViewport::~CViewport()
	{
		delete viewport;
	}

	void CViewport::update()
	{
		if(listChanged)
			drawAll();
		else
		{
			for(int i = 0; i < object_list.size(); i++)
				if(object_list[i]->hasChanged())
					drawObject(object_list[i]);
		}
	}

	void CViewport::clearViewport()
	{
		for(int x = 0; x < width; x++)
			for(int y = 0; y < height; y++)
				viewport[y][x] = ' ';
	}

	void CViewport::drawObject(IObject* obj)
	{
		obj->drawOut(viewport);
	}

	void CViewport::drawAll()
	{
		clearViewport();

		for(int i = 0; i < object_list.size(); i++)
			drawObject(object_list[i]);

		listChanged = false;
	}

	void CViewport::addToList(IObject* obj)
	{
		object_list.push_back(obj);
		listChanged = true;
	}

	void CViewport::removeFromList(IObject* obj)
	{
		for(int i = 0; i < object_list.size(); i++)
			if(object_list[i] == obj)
				object_list.erase(object_list.begin() + i);

		listChanged = true;
	}

	void CViewport::display()
	{
		system("cls");

		for(int y = 0; y < height; y++)
		{
			for(int x = 0; x < width; x++)
				std::cout << viewport[y][x];

			std::cout << std::endl;
		}
	}

	IObject* CViewport::addObject(Geometry* geometry, int collisionLvl)
	{
		IObject* obj = new CObject(*geometry, collisionLvl);

		// Check if an Object can be created at his position:

		for(int i = 0; i < obj->getPixelCount(); i++)
		{
			IObject* coll = getObjectByPoint(obj->getPixel(i)->_position, obj);

			if(coll != NULL)
			{
				std::ostringstream s;
				s << "Error!\nCannot register Object with ID = ";
				s << obj->getID();
				s << " (collisionLvl = ";
				s << obj->getCollisionLevel();

				s << ").\nReason: it collides with Object with ID = ";
				s << coll->getID();
				s << " (collisionLvl = ";
				s << coll->getCollisionLevel();
				s << ").";


				::MessageBoxA(NULL, s.str().c_str(), "ConGine", MB_OK | MB_ICONERROR);

				return NULL;
			}
		}

		obj->_register(this);
		
		return obj;
	}

	Rect<unsigned int> CViewport::getRect()
	{
		return rectangle;
	}

	unsigned int CViewport::getHeight()
	{
		return height;
	}

	unsigned int CViewport::getWidth()
	{
		return width;
	}

	char** CViewport::getVPTable()
	{
		return viewport;
	}

	IObject* CViewport::getObjectByPoint(Position2d<unsigned int> point, IObject* exception)
	{
		for(int i = 0; i < object_list.size(); i++)
			for(int x = 0; x < object_list[i]->getPixelCount(); x++)
				if(object_list[i]->getPixel(x)->_position._x == point._x &&
				   object_list[i]->getPixel(x)->_position._y == point._y && 
				   object_list[i] != exception )
				   return object_list[i];

		return NULL;
	}
};