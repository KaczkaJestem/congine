#include "Engine.h"
#include "CValueBox.h"
#include <conio.h>

/*
	Demo Code - not a part of the engine.
*/

using namespace Engine;

int main()
{
	// Create a new device - notice that we have to specify a separate rectangle for the viewport:
	IDevice* device = createDevice(Rect<unsigned int>(0, 0, 79, 24), Rect<unsigned int>(50, 1, 70, 23));

	if(!device)
		return -1;

	// Get the UI and Viewport pointers from the device object:
	IUserInterface * ui = device->getUserInterface();
	IViewport* vp = device->getViewport();

	// Here we add some basic gui elements:
	IButton* button = ui->addButton(Rect<unsigned int>(1, 1, 13, 1), "Click me!");
	IInputWindow* input = ui->addInputWindow(Rect<unsigned int>(1, 5, 20, 8), "Input");

	IProgressBar* progress_bar = ui->addProgressBar(Rect<unsigned int>(1, 10, 14, 10), 100, 80, 0);
	IValueBox<double>* value_box = ui->addValueBox(Rect<unsigned int>(12, 15, 17, 15), 3.14);
	ITextBox* text_box = ui->addTextBox(Rect<unsigned int>(1, 15, 10, 15), "Pi value");
	IOutputWindow* output = ui->addOutputWindow(Rect<unsigned int>(1, 20, 32, 20), " Welcome to ConGine!");

	// We specify the screen refresh rate.
	// But remember - it's only a command line based interface
	// and the whole viewport has to be redrawn each frame,
	// so be careful - too high frame rate may cause flickering.
	unsigned int desired_fps = 2;
	device->setFPSLimit(desired_fps);
	
	// Here goes tha main loop
	while(device)
	{
		// Perform an update each frame:
		ui->update();
		vp->update();

		// Draw the scene
		device->displayAll();
	}

	return 0;
}