#include "COutputWindow.h"

namespace Engine
{
	COutputWindow::COutputWindow(Rect<unsigned int> rect, std::string txt, int id, bool pickable)
		: IOutputWindow(rect, txt, id, pickable)
	{
	}

	COutputWindow::~COutputWindow()
	{
		_unregister(ui_ref);
	}

	void COutputWindow::makeActive(bool a)
	{

	}

	int COutputWindow::getID()
	{
		return id;
	}

	bool COutputWindow::isPickable()
	{
		return pickable;
	}

	void COutputWindow::setText(const char* txt)
	{
		text = txt;

		changed = true;
	}

	Rect<unsigned int> COutputWindow::getRect()
	{
		return rectangle;
	}

	std::string COutputWindow::getText()
	{
		return text;
	}

	void COutputWindow::_register(IUserInterface* _ui)
	{
		if(ui_ref != NULL)
			_unregister(_ui);

		ui_ref = _ui;

		_ui->addToList(this);
	}
	
	void COutputWindow::_unregister(IUserInterface* _ui)
	{
		ui_ref = NULL;

		_ui->removeFromList(this);
	}

	void COutputWindow::drawOut(char** table)
	{
		std::string _text = text;
		_text.insert(_text.begin(), '>');

		int i = 0;

		for(int y = rectangle._UpperLeftCorner._y; y <= rectangle._LowerRightCorner._y; y++)
		{
			table[y][rectangle._UpperLeftCorner._x] = ' ';

			for(int x = rectangle._UpperLeftCorner._x + 1; x < rectangle._LowerRightCorner._x; x++)
			{
				if(i < _text.size())
				{
					table[y][x] = _text[i];
					i++;
				}
				else
					table[y][x] = ' ';
			}

			table[y][rectangle._LowerRightCorner._x] = ' ';
		}

		changed = false;
	}

	bool COutputWindow::hasChanged()
	{
		return changed;
	}

	bool COutputWindow::isClipped()
	{
		if(!ui_ref)
			return true;

		unsigned int w = ui_ref->getWidth() - 1;
		unsigned int h = ui_ref->getHeight() - 1;

		if(rectangle._UpperLeftCorner._x < 0 || rectangle._LowerRightCorner._x > w ||
			rectangle._UpperLeftCorner._y < 0 || rectangle._LowerRightCorner._y > h)
			return true;

		return false;
	}
};