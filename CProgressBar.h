#pragma once

#include "IUserInterface.h"
#include "IProgressBar.h"

namespace Engine
{
	class CProgressBar : public IProgressBar
	{
		protected:
			virtual bool isClipped();

		public:
			CProgressBar(Rect<unsigned int> rect, int max, int curr, int min, int id = -1, bool pickable = false);
			~CProgressBar();

			virtual void		makeActive(bool a);
			virtual void		setMinValue(int value);
			virtual void		setMaxValue(int value);
			virtual void		setCurrValue(int value);

			virtual bool				isPickable();
			virtual int					getID();
			virtual int					getMinValue();
			virtual int					getCurrValue();
			virtual int					getMaxValue();
			virtual bool				hasChanged();
			virtual Rect<unsigned int>	getRect();
			virtual void				drawOut(char** table);

			virtual void _register(IUserInterface* _ui);
			virtual void _unregister(IUserInterface* _ui);

	};
};