#pragma once

#include <sstream>
#include "IUserInterface.h"
#include "IInputWindow.h"


namespace Engine
{
	class CInputWindow : public IInputWindow
	{
		protected:
			virtual bool isClipped();

		public:
			CInputWindow(Rect<unsigned int> rect, std::string text, int id = -1, bool pickable = true);
			~CInputWindow();

			virtual void				makeActive(bool a);
			virtual void				writeChar(char c);
			virtual void				setText(const char* txt);

			virtual std::string			getText();
			virtual int					getID();
			virtual bool				isPickable();
			virtual bool				hasChanged();
			virtual Rect<unsigned int>	getRect();
			virtual void				drawOut(char** table);

			virtual void _register(IUserInterface* _ui);
			virtual void _unregister(IUserInterface* _ui);

	};
};