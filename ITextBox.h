#pragma once

#include <string>
#include "IGUIElement.h"
#include "Rectangle.h"

namespace Engine
{
	class ITextBox abstract : public IGUIElement
	{
		protected:
			std::string text;

			virtual bool isClipped() = 0;

		public:
			ITextBox(Rect<unsigned int> rect, std::string txt, int _id = -1, bool _pickable = false)
				: IGUIElement(rect, _id, _pickable),
				  text(txt)
				{}

			virtual void makeActive(bool a) = 0;
			virtual void setText(const char* txt) = 0;
			
			virtual bool					isPickable() = 0;
			virtual int						getID() = 0;
			virtual std::string				getText() = 0;
			virtual Rect<unsigned int>		getRect() = 0;
			virtual void					drawOut(char** table) = 0;
	};
};