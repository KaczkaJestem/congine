#include "CObject.h"

namespace Engine
{
	CObject::CObject(Geometry _geometry, int _collisionLvl, int _id)
		: IObject(_geometry, _collisionLvl, _id)
	{
		
	}

	CObject::~CObject()
	{

	}

	void CObject::move(DIRECTION dir)
	{
			std::vector<Position2d<unsigned int>> g;
			
			for(int i = 0; i < geometry.size(); i++)
				g.push_back(geometry[i]._position);

			// "Move" an object:
				initMove(g, dir);

			for(int i = 0; i < g.size(); i++)
				if(!checkPoint(g[i]))
					return;

			cleanup(viewport_ref->getVPTable());

			for(int i = 0; i < geometry.size(); i++)
			{
				geometry[i]._position._x = g[i]._x;
				geometry[i]._position._y = g[i]._y;
			}

			for(int i = 0; i < geometry.size(); i++)
			{
				lastpos[i]._x = geometry[i]._position._x;
				lastpos[i]._y = geometry[i]._position._y;
			}

		changed = true;
	}

	void CObject::moveTo(Position2d<unsigned int> pos, int refpoint)
	{
		std::vector<Position2d<unsigned int>> g;
			
		for(int i = 0; i < geometry.size(); i++)
			g.push_back(geometry[i]._position);

		// "Move" an object:
			int relX = pos._x - g[refpoint]._x;
			int relY = pos._y - g[refpoint]._y;

			g[refpoint]._x = pos._x;
			g[refpoint]._y = pos._y;

			if(!checkPoint(g[refpoint]))
				return;

		for(int i = 0; i < g.size(); i++)
			if(i != refpoint)
			{
				g[i]._x += relX;
				g[i]._y += relY;

				if(!checkPoint(g[i]))
					return;
			}

		cleanup(viewport_ref->getVPTable());

		for(int i = 0; i < geometry.size(); i++)
		{
			geometry[i]._position._x = g[i]._x;
			geometry[i]._position._y = g[i]._y;
		}

		for(int i = 0; i < geometry.size(); i++)
		{
			lastpos[i]._x = geometry[i]._position._x;
			lastpos[i]._y = geometry[i]._position._y;
		}

		changed = true;
	}

	void CObject::setVisible(bool _visible)
	{
		visible = _visible;
		changed = true;
	}

	void CObject::setCollisionLevel(int _collisionLvl)
	{
		collisionLevel = _collisionLvl;
	}
	
	int CObject::getPixelCount()
	{
		return geometry.size();
	}
	
	Pixel<unsigned int>* CObject::getPixel(int num)
	{
		return &geometry[num];
	}

	int CObject::getID()
	{
		return id;
	}

	int CObject::getCollisionLevel()
	{
		return collisionLevel;
	}

	bool CObject::isVisible()
	{
		return visible;
	}

	bool CObject::hasChanged()
	{
		return changed;
	}


	void CObject::_register(IViewport* _vp)
	{
		if(viewport_ref != NULL)
			_unregister(_vp);

		viewport_ref = _vp;

		_vp->addToList(this);
	}
	
	void CObject::_unregister(IViewport* _vp)
	{
		viewport_ref = NULL;

		_vp->removeFromList(this);
	}
	
	void CObject::drawOut(char** table)
	{
		if(isClipped())
		{
			std::ostringstream s;
			s << "Error!\nObject with ID = ";
			s << id;
			s << " is out of Viewport! Please check the geometry.";

			::MessageBoxA(NULL, s.str().c_str(), "ConGine", MB_OK | MB_ICONERROR);
			return;
		}

		if(!visible)
			return;

		cleanup(table);
		
		for(int i = 0; i < geometry.size(); i++)
		{
			Pixel<unsigned int>* g = &geometry[i];
			table[g->_position._y][g->_position._x] = g->_char;
		}

		changed = false;
	}

	void CObject::cleanup(char** table)
	{
		for(int i = 0; i < geometry.size(); i++)
		{
			Pixel<unsigned int>* g = &geometry[i];
			table[g->_position._y][g->_position._x] = ' ';
		}

		changed = true;
	}

	bool CObject::isClipped()
	{
		if(!viewport_ref)
			return true;

		unsigned int w = viewport_ref->getWidth() - 1;
		unsigned int h = viewport_ref->getHeight() - 1;

		for(int i = 0; i < geometry.size(); i++)
			if(geometry[i]._position._x < 0 || geometry[i]._position._x > w ||
			   geometry[i]._position._y < 0 || geometry[i]._position._y > h)
				return true;

		return false;
	}

	bool CObject::checkPoint(Position2d<unsigned int> point)
	{
		if(!viewport_ref)
			return false;

		unsigned int w = viewport_ref->getWidth() - 1;
		unsigned int h = viewport_ref->getHeight() - 1;

		//Check if is inside the Viewport:

		if(point._x < 0 || point._x > w ||
		   point._y < 0 || point._y > h )
			return false;
		

		//Check if collides:

		IObject* obj = viewport_ref->getObjectByPoint(point, this);

		if(!obj)
			return true;
		
		if(obj->getCollisionLevel() >= this->getCollisionLevel())
			return false;

		return true;
	}

	void CObject::initMove(std::vector<Position2d<unsigned int>> & g, DIRECTION dir)
	{
		if (dir == DIR_UP)
		{
			for(int i = 0; i < g.size(); i++)
				g[i]._y --;
		}

		else if (dir == DIR_DOWN)
		{
			for(int i = 0; i < g.size(); i++)
				g[i]._y ++;
		}

		else if (dir == DIR_LEFT)
		{
			for(int i = 0; i < g.size(); i++)
				g[i]._x --;
		}

		else if (dir == DIR_RIGHT)		
		{
			for(int i = 0; i < g.size(); i++)
				g[i]._x ++;
		}

		else if (dir == DIR_LEFTUP)
		{
			for(int i = 0; i < g.size(); i++)
			{	g[i]._y --; g[i]._x --;	}
		}
		else if (dir == DIR_RIGHTUP)	
		{
			for(int i = 0; i < g.size(); i++)
			{	g[i]._y --;	g[i]._x ++;	}
		}
		else if (dir == DIR_LEFTDOWN)
		{
			for(int i = 0; i < g.size(); i++)
			{	g[i]._y ++;	g[i]._x --;	}
		}
		else if (dir == DIR_RIGHTDOWN)
		{
			for(int i = 0; i < g.size(); i++)
			{	g[i]._y ++;	g[i]._x ++;	}
		}
	}
};