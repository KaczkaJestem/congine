#pragma once

namespace Engine
{
	class ICommand abstract
	{
		public:
			virtual void execute() = 0;
	};
};
