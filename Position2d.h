#pragma once

namespace Engine
{

template <class T>
class Position2d
{
public:

	Position2d(T x, T y)
		: _x(x),
		  _y(y)	{}

	T _x;
	T _y;
};

};