#include "Rectangle.h"


template <class T>
Engine::Rect<T>::Rect(Position2d<T> UpperLeftCorner, Position2d<T> LowerRightCorner)
	: _UpperLeftCorner(UpperLeftCorner),
	  _LowerRightCorner(LowerRightCorner)
{
}

template <class T>
Engine::Rect<T>::Rect(T ulc_x, T ulc_y, T drc_x, T drc_y)
	: _UpperLeftCorner(Position2d<T>(ulc_x, ulc_y)),
	  _LowerRightCorner(Position2d<T>(drc_x, drc_y))
{
}

template <class T>
T Engine::Rect<T>::getWidth()
{
	return _LowerRightCorner._x - _UpperLeftCorner._x;
}

template <class T>
T Engine::Rect<T>::getHeight()
{
	return _LowerRightCorner._y - _UpperLeftCorner._y;
}