#include "CUserInterface.h"

namespace Engine
{
	/*
		Used to create a pointer to new CUserInterface instance:
	*/

	IUserInterface* createUserInterface(Rect<unsigned int> _WindowRect, Rect<unsigned int> _ViewportRect, char _uiBackgrChar)
	{
		return new CUserInterface(_WindowRect, _ViewportRect, _uiBackgrChar);
	}

	/*
		CUserInterface class definition:
	*/

	CUserInterface::CUserInterface(Rect<unsigned int> ui, Rect<unsigned int> viewport, char uiBackgrChar)
		: IUserInterface(ui, viewport, uiBackgrChar),
		  ui_rect(ui),
		  viewport_rect(viewport)
	{
		clearUI();
	}

	CUserInterface::~CUserInterface()
	{

	}

	void CUserInterface::addToList(IGUIElement* elem)
	{
		element_list.push_back(elem);

		if(elem->isPickable())
		{
			elem->makeActive(true);
			active = elem;
		}

		listChanged = true;
	}

	void CUserInterface::removeFromList(IGUIElement* elem)
	{
		for(int i = 0; i < element_list.size(); i++)
			if(element_list[i] == elem)
				element_list.erase(element_list.begin() + i);

		if(elem == active)
			active = NULL;

		listChanged = true;
	}

	void CUserInterface::drawElement(Engine::IGUIElement* elem)
	{
		elem->drawOut(IUserInterface::ui);
	}

	void CUserInterface::drawAll()
	{
		clearUI();

		for(int i = 0; i < element_list.size(); i++)
			drawElement(element_list[i]);

		listChanged = false;
	}

	void CUserInterface::update()
	{
		if(listChanged)
			drawAll();
		else
		{
			// JUST UPDATE VALUE BOXES / OUTPUT WINDOWS / PROGRESS BARS ETC.
			for(int i = 0; i < element_list.size(); i++)
				if(element_list[i]->hasChanged())
					drawElement(element_list[i]);
		}
	}

	void CUserInterface::clearUI()
	{
		for(unsigned int x = 0; x < width; x++)
			for(unsigned int y = 0; y < height; y++)
				IUserInterface::ui[y][x] = backgr;
	}

	void CUserInterface::setSkin(char c)
	{
		backgr = c;
		clearUI();
		update();
	}

	void CUserInterface::display()
	{
		system("cls");

		unsigned int y1 = ui_rect._UpperLeftCorner._y;
		unsigned int x1 = ui_rect._UpperLeftCorner._x;

		for(int y = 0; y < height; y++)
		{
			if(y >= y1)
			{
				for(int x = 0; x < width; x++)
					if(x >= x1)
						std::cout << ui[y - y1][x - x1];
					else
						std::cout << ' ';
			}
			else
			{
				for(int x = 0; x < width; x++)
					std::cout << ' ';
			}

			std::cout << std::endl;
		}
	}

	void CUserInterface::makeNextElementActive()
	{
		int a = 0;

		for(; a < element_list.size(); a++)
			if(element_list[a] == active)
				break;
		
		int i = a + 1;

		while( i != a )
		{
			if( i >= element_list.size())
				i = 0;

			if(element_list[i]->isPickable())
			{
				active->makeActive(false);
				active = element_list[i];
				active->makeActive(true);

				return;
			}

			i++;
		}
	}

	void CUserInterface::makePrevElementActive()
	{
		int a = 0;

		for(; a < element_list.size(); a++)
			if(element_list[a] == active)
				break;
		
		int i = a - 1;

		while( i != a )
		{
			if( i < 0)
				i = element_list.size() - 1;

			if(element_list[i]->isPickable())
			{
				active->makeActive(false);
				active = element_list[i];
				active->makeActive(true);

				return;
			}

			i--;
		}
	}

	IGUIElement* CUserInterface::getActiveElement()
	{
		return active;
	}

	Rect<unsigned int> CUserInterface::getRect()
	{
		return ui_rect;
	}

	unsigned int CUserInterface::getHeight()
	{
		return height;
	}

	unsigned int CUserInterface::getWidth()
	{
		return width;
	}

	void CUserInterface::blendDisplay(char** vp)
	{
		system("cls");

		unsigned int y1 = ui_rect._UpperLeftCorner._y;
		unsigned int x1 = ui_rect._UpperLeftCorner._x;

		Position2d<unsigned int> lr = viewport_rect._LowerRightCorner;
		Position2d<unsigned int> ul = viewport_rect._UpperLeftCorner;

		for(int y = 0; y < height; y++)
		{
			if(y >= y1)
			{
				for(int x = 0; x < width; x++)
				{
					if(x >= x1)
					{
						if(!(x >= ul._x && x < lr._x && y >= ul._y && y < lr._y))
							std::cout << IUserInterface::ui[y][x];
						else
							std::cout << vp[y - ul._y][x - ul._x];
					}
					else
						std::cout << ' ';
				}
			}
			else
			{
				for(int x = 0; x < width; x++)
					std::cout << ' ';
			}

			std::cout << std::endl;
		}
	}

	ITextBox* CUserInterface::addTextBox(Rect<unsigned int> rect, const char* txt, int id, bool pickable)
	{
		std::ostringstream s;
		s << txt;

		CTextBox* tb = new CTextBox(rect, s.str(), id, pickable);
		tb->_register(this);

		return tb;
	}

	IValueBox<int>* CUserInterface::addValueBox(Rect<unsigned int> rect, int value, int id, bool pickable)
	{
		IValueBox<int>* vb = new CValueBox<int>(rect, value, id, pickable);
		vb->_register(this);

		return vb;
	}

	IValueBox<unsigned int>* CUserInterface::addValueBox(Rect<unsigned int> rect, unsigned int value, int id, bool pickable)
	{
		IValueBox<unsigned int>* vb = new CValueBox<unsigned int>(rect, value, id, pickable);
		vb->_register(this);

		return vb;
	}

	IValueBox<short int>* CUserInterface::addValueBox(Rect<unsigned int> rect, short int value, int id, bool pickable)
	{
		IValueBox<short int>* vb = new CValueBox<short int>(rect, value, id, pickable);
		vb->_register(this);

		return vb;
	}

	IValueBox<float>* CUserInterface::addValueBox(Rect<unsigned int> rect, float value, int id, bool pickable)
	{
		IValueBox<float>* vb = new CValueBox<float>(rect, value, id, pickable);
		vb->_register(this);

		return vb;
	}

	IValueBox<double>* CUserInterface::addValueBox(Rect<unsigned int> rect, double value, int id, bool pickable)
	{
		IValueBox<double>* vb = new CValueBox<double>(rect, value, id, pickable);
		vb->_register(this);

		return vb;
	}

	IValueBox<char>* CUserInterface::addValueBox(Rect<unsigned int> rect, char value, int id, bool pickable)
	{
		IValueBox<char>* vb = new CValueBox<char>(rect, value, id, pickable);
		vb->_register(this);

		return vb;
	}

	IValueBox<bool>* CUserInterface::addValueBox(Rect<unsigned int> rect, bool value, int id, bool pickable)
	{
		IValueBox<bool>* vb = new CValueBox<bool>(rect, value, id, pickable);
		vb->_register(this);

		return vb;
	}

	IOutputWindow* CUserInterface::addOutputWindow(Rect<unsigned int> rect, const char* txt, int id, bool pickable)
	{
		std::ostringstream s;
		s << txt;

		IOutputWindow* ow = new COutputWindow(rect, s.str(), id, pickable);
		ow->_register(this);

		return ow;
	}

	IInputWindow* CUserInterface::addInputWindow(Rect<unsigned int> rect, const char* txt, int id, bool pickable)
	{
		std::ostringstream s;
		s << txt;

		IInputWindow* iw = new CInputWindow(rect, s.str(), id, pickable);
		iw->_register(this);

		return iw;
	}

	IButton* CUserInterface::addButton(Rect<unsigned int> rect, const char* txt, int id, bool pickable, bool checked)
	{
		std::ostringstream s;
		s << txt;

		IButton* b = new CButton(rect, s.str(), id, pickable, checked);
		b->_register(this);

		return b;
	}

	IProgressBar* CUserInterface::addProgressBar(Rect<unsigned int> rect, int maxValue, int currValue, int minValue, int id, bool pickable)
	{
		IProgressBar* pb = new CProgressBar(rect, maxValue, currValue, minValue, id, pickable);
		pb->_register(this);

		return pb;
	}
};