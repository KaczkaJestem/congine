#pragma once

#include <iostream>
#include <windows.h>
#include "IViewport.h"
#include "CObject.h"

namespace Engine
{
	/*
		Creates pointer to a new instance of Viewport
	*/

	IViewport* createViewport(Rect<unsigned int> _rectangle);

	
	class CViewport : public IViewport
	{
		public:
			CViewport(Rect<unsigned int> _rectangle);
			~CViewport();

			virtual void update();
			virtual void clearViewport();
			virtual void drawObject(IObject* obj);
			virtual void drawAll();
			virtual void addToList(IObject* obj);
			virtual void removeFromList(IObject* obj);
			virtual void display();

			virtual IObject* addObject(Geometry* geometry, int collisionLvl);

			virtual Rect<unsigned int>	getRect();
			virtual unsigned int		getHeight();
			virtual unsigned int		getWidth();

			virtual char**				getVPTable();
			virtual IObject*			getObjectByPoint(Position2d<unsigned int> point, IObject* exception = NULL);
	};

};