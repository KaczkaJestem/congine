#pragma once

#include "Pixel.h"

namespace Engine
{
	typedef std::vector<Pixel<unsigned int>> Geometry;
	
	Geometry* createGeometry(Pixel<unsigned int>* pixels, int pixelsCount);
};