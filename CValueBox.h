#pragma once

#include <cstring>
#include "IUserInterface.h"
#include "IValueBox.h"

namespace Engine
{
	template <class T>
	class CValueBox : public IValueBox<T>
	{
		protected:
			virtual bool isClipped()
			{
				if(!ui_ref)
					return true;

				unsigned int w = ui_ref->getWidth() - 1;
				unsigned int h = ui_ref->getHeight() - 1;

				if(rectangle._UpperLeftCorner._x < 0 || rectangle._LowerRightCorner._x > w ||
					rectangle._UpperLeftCorner._y < 0 || rectangle._LowerRightCorner._y > h)
					return true;

				return false;
			}

		public:
			CValueBox(Rect<unsigned int> rect, T value, int id = -1, bool pickable = false)
				: IValueBox<T>(rect, value, id, pickable)
			{
	
			}

			~CValueBox()
			{
				_unregister(ui_ref);
			}

			virtual void setValue(T _value)
			{
				value = _value;
	
				changed = true;
			}

			virtual int	getID()
			{
				return id;
			}

			virtual void makeActive(bool a)
			{
				
			}

			virtual bool isPickable()
			{
				return pickable;
			}

			virtual bool hasChanged()
			{
				return changed;
			}

			virtual Rect<unsigned int> getRect()
			{
				return rectangle;
			}

			virtual T getValue()
			{
				return value;
			}
			
			virtual void drawOut(char** table)
			{
				std::ostringstream text;
				text << value;
				char* c = strdup(text.str().c_str());

				int i = 0;

				for(int y = rectangle._UpperLeftCorner._y; y <= rectangle._LowerRightCorner._y; y++)
				{
					table[y][rectangle._UpperLeftCorner._x] = ' ';

					for(int x = rectangle._UpperLeftCorner._x + 1; x < rectangle._LowerRightCorner._x; x++)
					{
						text.seekp(0, std::ios_base::end);
						int size = text.tellp();

						if(i < size)
						{
							table[y][x] = c[i];
							i++;
						}
						else
							table[y][x] = ' ';
					}

					table[y][rectangle._LowerRightCorner._x] = ' ';
				}

				changed = false;
			}

			

			virtual void _register(IUserInterface* _ui)
			{
				if(ui_ref != NULL)
					_unregister(_ui);

				ui_ref = _ui;

				_ui->addToList(this);
			}

			virtual void _unregister(IUserInterface* _ui)
			{
				ui_ref = NULL;

				_ui->removeFromList(this);
			}
	};
};