#pragma once

#include "Position2d.h"

namespace Engine
{

template <class T>
class Rect
{
public: 

	Rect(Position2d<T> UpperLeftCorner, Position2d<T> LowerRightCorner);

	Rect(T ulc_x, T ulc_y, T drc_x, T drc_y);

	Position2d<T> _UpperLeftCorner;
	Position2d<T> _LowerRightCorner;

	T getWidth();
	T getHeight();
};

#include "Rectangle.cpp"
};