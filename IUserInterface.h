#pragma once

#include <vector>
#include "Rectangle.h"
#include "IGUIElement.h"

#include "ITextBox.h"
#include "IValueBox.h"
#include "IProgressBar.h"
#include "IOutputWindow.h"
#include "IInputWindow.h"
#include "IButton.h"


namespace Engine
{
	class IUserInterface abstract
	{
		protected:
			std::vector <IGUIElement*> element_list;
			Rect <unsigned int> ui_rect;
			Rect <unsigned int> viewport_rect;

			unsigned int width;
			unsigned int height;

			bool listChanged;

			char** ui;
			char backgr;

			IGUIElement* active;

		public:
			IUserInterface(Rect<unsigned int> ui, Rect<unsigned int> viewport, char uiBackgrChar) 
				: ui_rect(ui),
				  listChanged(false),
				  viewport_rect(viewport),
				  backgr(uiBackgrChar),
				  active(NULL)
			{
				width = ui_rect.getWidth();
				height = ui_rect.getHeight();

				IUserInterface::ui = new char* [height];
				 
				for(unsigned int i = 0; i < height; i++)
					IUserInterface::ui[i] = new char[width];
			}

			~IUserInterface() {}

			virtual void update() = 0;
			virtual void clearUI() = 0;
			virtual void setSkin(char c) = 0;
			virtual void addToList(IGUIElement* elem) = 0;
			virtual void removeFromList(IGUIElement* elem) = 0;
			virtual void drawElement(IGUIElement* elem) = 0;
			virtual void drawAll() = 0;
			virtual void display() = 0;
			virtual void blendDisplay(char** vp) = 0;
			virtual void makeNextElementActive() = 0;
			virtual void makePrevElementActive() = 0;


			// Feel free to override:

			virtual IValueBox<int>*				addValueBox(Rect<unsigned int> rect, int value, int id = -1, bool pickable = false) = 0;
			virtual IValueBox<unsigned int>*	addValueBox(Rect<unsigned int> rect, unsigned int value, int id = -1, bool pickable = false) = 0;
			virtual IValueBox<short int>*		addValueBox(Rect<unsigned int> rect, short int value, int id = -1, bool pickable = false) = 0;
			virtual IValueBox<float>*			addValueBox(Rect<unsigned int> rect, float value, int id = -1, bool pickable = false) = 0;
			virtual IValueBox<double>*			addValueBox(Rect<unsigned int> rect, double value, int id = -1, bool pickable = false) = 0;
			virtual IValueBox<char>*			addValueBox(Rect<unsigned int> rect, char value, int id = -1, bool pickable = false) = 0;
			virtual IValueBox<bool>*			addValueBox(Rect<unsigned int> rect, bool value, int id = -1, bool pickable = false) = 0;

			virtual ITextBox*			addTextBox(Rect<unsigned int> rect, const char* txt, int id = -1, bool pickable = false) = 0;
			virtual IOutputWindow*		addOutputWindow(Rect<unsigned int> rect, const char* txt, int id = -1, bool pickable = false) = 0;
			virtual IInputWindow*		addInputWindow(Rect<unsigned int> rect, const char* txt, int id = -1, bool pickable = true) = 0;
			virtual IButton*			addButton(Rect<unsigned int> rect, const char* txt, int id = -1, bool pickable = true, bool checked = false) = 0;
			virtual IProgressBar*		addProgressBar(Rect<unsigned int> rect, int maxVaule, int currValue, int minValue, int id = -1, bool pickable = false) = 0;

			virtual Rect<unsigned int>	getRect() = 0;
			virtual unsigned int		getHeight() = 0;
			virtual unsigned int		getWidth() = 0;
			virtual IGUIElement*		getActiveElement() = 0;
	};
};