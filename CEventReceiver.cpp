#include "CEventReceiver.h"

namespace Engine
{
	CEventReceiver::CEventReceiver()
		: IEventReceiver()
	{

	}

	CEventReceiver::~CEventReceiver()
	{

	}

	void CEventReceiver::update()
	{
		if(!ui)
			return;

		char c = '\0';

		if(_kbhit())
		{	
			c = _getch();
		}

		// Switch between buttons:
		if(isKeyPressed(VK_TAB))
			ui->makeNextElementActive();

		IGUIElement* caller = ui->getActiveElement();

		if(caller != NULL)
		{
			// Here place your GUIElement's events handling:

			
			if(IInputWindow* _iw = dynamic_cast<IInputWindow*>(caller))
			{
				// Here goes the IInputWindow handling:

				IInputWindow* iw = _iw;

				if(c != '\0')
				iw->writeChar(c);
			}

			if(IButton* _b = dynamic_cast<IButton*> (caller))
			{
				// Here goes the IButton handling:	

				switch(_b->getID())
				{

				}
			}
		}
	}

	bool CEventReceiver::isKeyPressed(int keyCode)
	{
		return GetAsyncKeyState(keyCode);	
	}

	void CEventReceiver::setUserInterface(IUserInterface* _ui)
	{
		ui = _ui;
	}
};