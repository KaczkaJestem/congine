#pragma once

#include <iostream>
#include <windows.h>
#include "IUserInterface.h"

#include "CTextBox.h"
#include "CValueBox.h"
#include "CProgressBar.h"
#include "COutputWindow.h"
#include "CInputWindow.h"
#include "CButton.h"


namespace Engine
{
	class CUserInterface : public IUserInterface
	{
		protected:
			std::vector <IGUIElement*> element_list;
			Rect <unsigned int> ui_rect;
			Rect <unsigned int> viewport_rect;

			char** ui;

		public:
			CUserInterface(Rect<unsigned int> ui, Rect<unsigned int> viewport, char uiBackgrChar);
			~CUserInterface();

			virtual void update();
			virtual void clearUI();
			virtual void setSkin(char c);
			virtual void addToList(IGUIElement* elem);
			virtual void removeFromList(IGUIElement* elem);
			virtual void drawElement(IGUIElement* elem);
			virtual void drawAll();
			virtual void display();
			virtual void blendDisplay(char** vp);
			virtual void makeNextElementActive();
			virtual void makePrevElementActive();

			
			// Feel free to override:

			virtual IValueBox<int>*				addValueBox(Rect<unsigned int> rect, int value, int id = -1, bool pickable = false);
			virtual IValueBox<unsigned int>*	addValueBox(Rect<unsigned int> rect, unsigned int value, int id = -1, bool pickable = false);
			virtual IValueBox<short int>*		addValueBox(Rect<unsigned int> rect, short int value, int id = -1, bool pickable = false);
			virtual IValueBox<float>*			addValueBox(Rect<unsigned int> rect, float value, int id = -1, bool pickable = false);
			virtual IValueBox<double>*			addValueBox(Rect<unsigned int> rect, double value, int id = -1, bool pickable = false);
			virtual IValueBox<char>*			addValueBox(Rect<unsigned int> rect, char value, int id = -1, bool pickable = false);
			virtual IValueBox<bool>*			addValueBox(Rect<unsigned int> rect, bool value, int id = -1, bool pickable = false);

			virtual ITextBox*				addTextBox(Rect<unsigned int> rect, const char* txt, int id = -1, bool pickable = false);
			virtual IOutputWindow*			addOutputWindow(Rect<unsigned int> rect, const char* txt, int id = -1, bool pickable = false);
			virtual IInputWindow*			addInputWindow(Rect<unsigned int> rect, const char* txt, int id = -1, bool pickable = false);
			virtual IButton*				addButton(Rect<unsigned int> rect, const char* txt, int id = -1, bool pickable = false, bool checked = false);
			virtual IProgressBar*			addProgressBar(Rect<unsigned int> rect, int maxVaule, int currValue, int minValue, int id = -1, bool pickable = false);

			virtual Rect<unsigned int>	getRect();
			virtual unsigned int		getHeight();
			virtual unsigned int		getWidth();
			virtual IGUIElement*		getActiveElement();
	};

	IUserInterface* createUserInterface(Rect<unsigned int> _WindowRect, Rect<unsigned int> _ViewportRect, char _uiBackgrChar);
};