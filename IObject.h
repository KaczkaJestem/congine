#pragma once

#include <vector>
#include "Pixel.h"
#include "Geometry.h"
#include "Position2d.h"
#include "Direction.h"

namespace Engine
{
	class IViewport;

	class IObject abstract
	{
		protected:
			Geometry								geometry;
			std::vector<Position2d<unsigned int>>	lastpos;
			IViewport*								viewport_ref;
			int collisionLevel;
			bool visible;
			// True if a change was made but not rendered yet:
			bool changed;
			int	id;

			virtual bool isClipped() = 0;
			virtual bool checkPoint(Position2d<unsigned int> point) = 0;
			virtual void initMove(std::vector<Position2d<unsigned int>> & g, DIRECTION dir) = 0;

		public:
			IObject(Geometry _geometry, int _collisionLvl, int _id = -1)
				: geometry(_geometry),
				  collisionLevel(_collisionLvl),
				  visible(true),
				  changed(false),
				  viewport_ref(NULL),
				  id(_id)
			{
				for(unsigned int i = 0; i < geometry.size(); i++)
					lastpos.push_back(geometry[i]._position);
			}

			~IObject() {}

			virtual void move(DIRECTION dir) = 0;
			virtual void moveTo(Position2d<unsigned int> pos, int refpoint = 0) = 0;
			virtual void setVisible(bool visible) = 0;
			virtual void setCollisionLevel(int collisionLvl) = 0;
			
			virtual int						getPixelCount() = 0;
			virtual Pixel<unsigned int>*	getPixel(int num) = 0;
			virtual int						getCollisionLevel() = 0;
			virtual int						getID() = 0;
			virtual bool					isVisible() = 0;
			virtual bool					hasChanged() = 0;

			virtual void					drawOut(char** table) = 0;
			virtual void					cleanup(char** table) = 0;

			virtual void _register(IViewport* _vp) = 0;
			virtual void _unregister(IViewport* _vp) = 0;
	};

};