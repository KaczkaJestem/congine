enum DIRECTION
{
	DIR_UP,
	DIR_DOWN,
	DIR_LEFT,
	DIR_RIGHT,

	DIR_LEFTUP,
	DIR_RIGHTUP,
	DIR_LEFTDOWN,
	DIR_RIGHTDOWN
};