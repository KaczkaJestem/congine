#pragma once

#include <windows.h>
#include "IViewport.h"
#include "IObject.h"

namespace Engine
{
	class CObject : public IObject
	{
		protected:
			virtual bool isClipped();
			virtual bool checkPoint(Position2d<unsigned int> point);
			virtual void initMove(std::vector<Position2d<unsigned int>> & g, DIRECTION dir);

		public:
			CObject(Geometry _geometry, int _collisionLvl, int _id = -1);
			~CObject();

			virtual void move(DIRECTION dir);
			virtual void moveTo(Position2d<unsigned int> pos, int refpoint = 0);
			virtual void setVisible(bool visible);
			virtual void setCollisionLevel(int collisionLvl);
			
			virtual int						getPixelCount();
			virtual Pixel<unsigned int>*	getPixel(int num);
			virtual int						getCollisionLevel();
			virtual int						getID();
			virtual bool					isVisible();
			virtual bool					hasChanged();

			virtual void					drawOut(char** table);
			virtual void					cleanup(char** table);

			virtual void					_register(IViewport* _vp);
			virtual void					_unregister(IViewport* _vp);
	};

};