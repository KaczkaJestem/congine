#pragma once

#include <time.h>
#include "IEventReceiver.h"
#include "IViewport.h"

namespace Engine
{

class IDevice abstract
{
protected:
	IViewport* viewport;
	IUserInterface* ui;
	IEventReceiver* receiver;

	// Used for FPS calculations:

	int _fps_counter;
	double _delta_frame_limit;
	int FPS;
	clock_t _lastframetime;
	clock_t _lastfpstime;

public:
	IDevice()
		: _fps_counter(0),
		  _delta_frame_limit(0),
		  _lastframetime(0),
		  _lastfpstime(0),
		  FPS(-1),
		  viewport(nullptr),
		  ui(nullptr),
		  receiver(nullptr)
	{}

	~IDevice() {}

	virtual void stopDevice() = 0;

	virtual IUserInterface* getUserInterface() = 0;
	virtual IViewport*		getViewport() = 0;
	virtual IEventReceiver*	getEventReceiver() = 0;

	virtual int				getFPS() = 0;
	virtual void			setFPSLimit(unsigned int limit) = 0;

	virtual void			setEventReceiver(IEventReceiver* _receiver) = 0;

	virtual void			displayAll() = 0;
};



/* Creates the Engine's device and returns a pointer to it's instance or NULL
	if failed to create.
	Note: Viewport rectangle must be within Window rectangle.
*/

IDevice* createDevice(Rect<unsigned int> _WindowRect, Rect<unsigned int> _ViewportRect);


};
