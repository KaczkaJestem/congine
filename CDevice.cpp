#include "CDevice.h"

namespace Engine
{

	IDevice* createDevice(Rect<unsigned int> _WindowRect, Rect<unsigned int> _ViewportRect)
	{
		/*
			Check if the Viewport rectangle is within the Window rectangle:
		*/

		if	(_ViewportRect._UpperLeftCorner._x < _WindowRect._UpperLeftCorner._x
			||	_ViewportRect._UpperLeftCorner._y < _WindowRect._UpperLeftCorner._y
			||	_ViewportRect._LowerRightCorner._x > _WindowRect._LowerRightCorner._x
			||	_ViewportRect._LowerRightCorner._y > _WindowRect._LowerRightCorner._y)
		{
			::MessageBoxA(NULL, "Error creating device: check UI and Viewport rectangles!", "ConGine", MB_OK | MB_ICONERROR);
			return NULL;
		}

		
		return new CDevice(_WindowRect, _ViewportRect);
	}


	/*
		CDevice class definition:
	*/

	CDevice::CDevice(Engine::Rect<unsigned int> _WindowRect, Engine::Rect<unsigned int> _ViewportRect, char uiBackgrChar)
		: IDevice()
	{
		viewport = createViewport(_ViewportRect);
		ui = createUserInterface(_WindowRect, _ViewportRect, uiBackgrChar);
	}

	CDevice::~CDevice()
	{
		stopDevice();
	}

	void CDevice::stopDevice()
	{
		delete viewport;
		delete ui;
	}

	IUserInterface* CDevice::getUserInterface()
	{
		return ui;
	}

	IViewport* CDevice::getViewport()
	{
		return viewport;
	}

	IEventReceiver* CDevice::getEventReceiver()
	{
		return receiver;
	}

	int CDevice::getFPS()
	{
		return FPS;
	}

	void CDevice::setFPSLimit(unsigned int limit)
	{
		if (limit == 0)
			_delta_frame_limit = 0;
		else
			_delta_frame_limit = (1.0 / limit) * CLOCKS_PER_SEC;
	}

	void CDevice::displayAll()
	{	
		clock_t now = clock();

		if ((now - _lastframetime) >= _delta_frame_limit)
		{
			ui->blendDisplay(viewport->getVPTable());

			_fps_counter++;

			if ((now - _lastfpstime) / CLOCKS_PER_SEC >= 1)
			{
				FPS = _fps_counter;
				_fps_counter = 0;
				_lastfpstime = now;
			}

			_lastframetime = now;
		}
	}

	void CDevice::setEventReceiver(IEventReceiver* _receiver)
	{
		receiver = _receiver;
		receiver->setUserInterface(ui);
	}
};