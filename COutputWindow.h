#pragma once

#include "IUserInterface.h"
#include "IOutputWindow.h"

namespace Engine
{
	class COutputWindow : public IOutputWindow
	{
		protected:
			virtual bool isClipped();

		public:
			COutputWindow(Rect<unsigned int> rect, std::string txt, int id = -1, bool pickable = false);
			~COutputWindow();

			virtual void setText(const char* txt);

			virtual void					makeActive(bool a);
			virtual int						getID();
			virtual bool					isPickable();
			virtual bool					hasChanged();
			virtual Rect<unsigned int>		getRect();
			virtual std::string				getText();
			virtual void					drawOut(char** table);

			virtual void _register(IUserInterface* _ui);
			virtual void _unregister(IUserInterface* _ui);
	};
};