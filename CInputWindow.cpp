#include "CInputWindow.h"


namespace Engine
{
	CInputWindow::CInputWindow(Rect<unsigned int> rect, std::string text, int id, bool pickable)
		: IInputWindow(rect, text, id, pickable)
	{

	}

	CInputWindow::~CInputWindow()
	{

	}

	void CInputWindow::makeActive(bool a)
	{		
		active = a;
		changed = true;
	}

	void CInputWindow::writeChar(char c)
	{
		if(c == '\b')
		{
			if(text.size() > 0)
			text.erase(text.end() - 1);
		}


		else if (c == '\r')
			/* Do nothing. */;
		else if (c == '\t')
			/* Do nothing. */;
		else
			text += c;

		changed = true;
	}
	
	void CInputWindow::setText(const char* txt)
	{
		std::ostringstream s;
		s << txt;

		text = s.str();

		changed = true;
	}

	std::string CInputWindow::getText()
	{
		return text;
	}

	int CInputWindow::getID()
	{
		return id;
	}

	bool CInputWindow::isPickable()
	{
		return pickable;
	}

	bool CInputWindow::hasChanged()
	{
		return changed;
	}

	Rect<unsigned int> CInputWindow::getRect()
	{
		return rectangle;
	}

	void CInputWindow::drawOut(char** table)
	{
		std::string _text = text;

		if(active)
		{
			_text += "_";
		}

		int i = 0;

		for(int y = rectangle._UpperLeftCorner._y; y <= rectangle._LowerRightCorner._y; y++)
		{
			table[y][rectangle._UpperLeftCorner._x] = ' ';

			for(int x = rectangle._UpperLeftCorner._x + 1; x < rectangle._LowerRightCorner._x; x++)
			{
				if(i < _text.size())
				{
					table[y][x] = _text[i];
					i++;
				}
				else
					table[y][x] = ' ';
			}

			table[y][rectangle._LowerRightCorner._x] = ' ';
		}

		changed = false;
	}

	void CInputWindow::_register(IUserInterface* _ui)
	{
		if(ui_ref != NULL)
			_unregister(_ui);

		ui_ref = _ui;

		_ui->addToList(this);
	}

	void CInputWindow::_unregister(IUserInterface* _ui)
	{
		ui_ref = NULL;

		_ui->removeFromList(this);
	}

	bool CInputWindow::isClipped()
	{
		if(!ui_ref)
			return true;

		unsigned int w = ui_ref->getWidth() - 1;
		unsigned int h = ui_ref->getHeight() - 1;

		if(rectangle._UpperLeftCorner._x < 0 || rectangle._LowerRightCorner._x > w ||
			rectangle._UpperLeftCorner._y < 0 || rectangle._LowerRightCorner._y > h)
			return true;

		return false;
	}
};