#include "CProgressBar.h"

namespace Engine
{
	CProgressBar::CProgressBar(Rect<unsigned int> rect, int max, int curr, int min, int id, bool pickable)
		: IProgressBar(rect, max, curr, min, id, pickable)
	{

	}

	CProgressBar::~CProgressBar()
	{
		_unregister(ui_ref);
	}

	void CProgressBar::makeActive(bool a)
	{

	}

	int CProgressBar::getID()
	{
		return id;
	}

	bool CProgressBar::isPickable()
	{
		return pickable;
	}

	void CProgressBar::setMinValue(int value)
	{
		minValue = value;

		if(currValue < minValue)
			setCurrValue(minValue);
		if(maxValue < minValue)
			setMaxValue(minValue);

		changed = true;
	}

	void CProgressBar::setMaxValue(int value)
	{
		maxValue = value;

		if(currValue > maxValue)
			setCurrValue(maxValue);
		if(minValue > maxValue)
			setMinValue(maxValue);

		changed = true;
	}

	void CProgressBar::setCurrValue(int value)
	{
		currValue = value;

		if(currValue < minValue)
			setCurrValue(minValue);
		if(currValue > maxValue)
			setCurrValue(maxValue);

		changed = true;
	}

	int CProgressBar::getMinValue()
	{
		return minValue;
	}

	int CProgressBar::getCurrValue()
	{
		return currValue;
	}

	int	CProgressBar::getMaxValue()
	{
		return maxValue;
	}

	Rect<unsigned int> CProgressBar::getRect()
	{
		return rectangle;
	}

	void CProgressBar::_register(IUserInterface* _ui)
	{
		if(ui_ref != NULL)
			_unregister(_ui);

		ui_ref = _ui;

		_ui->addToList(this);
	}
	
	void CProgressBar::_unregister(IUserInterface* _ui)
	{
		ui_ref = NULL;

		_ui->removeFromList(this);
	}

	void CProgressBar::drawOut(char** table)
	{
		int lines = (double (currValue - minValue) / double(maxValue - minValue)) * 10.0;

		std::string text = "[";
		for(int p = 0; p < 10; p++)
			text += (p < lines)? "|" : " ";

		text += "]";

		int i = 0;

		for(int y = rectangle._UpperLeftCorner._y; y <= rectangle._LowerRightCorner._y; y++)
		{
			table[y][rectangle._UpperLeftCorner._x] = ' ';

			for(int x = rectangle._UpperLeftCorner._x + 1; x < rectangle._LowerRightCorner._x; x++)
			{
				if(i < text.size())
				{
					table[y][x] = text[i];
					i++;
				}
				else
					table[y][x] = ' ';
			}

			table[y][rectangle._LowerRightCorner._x] = ' ';
		}

		changed = false;
	}

	bool CProgressBar::hasChanged()
	{
		return changed;
	}

	bool CProgressBar::isClipped()
	{
		if(!ui_ref)
			return true;

		unsigned int w = ui_ref->getWidth() - 1;
		unsigned int h = ui_ref->getHeight() - 1;

		if(rectangle._UpperLeftCorner._x < 0 || rectangle._LowerRightCorner._x > w ||
			rectangle._UpperLeftCorner._y < 0 || rectangle._LowerRightCorner._y > h)
			return true;

		return false;
	}
};