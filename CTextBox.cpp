#include "CTextBox.h"


namespace Engine
{
	CTextBox::CTextBox(Rect<unsigned int> rect, std::string txt, int id, bool pickable)
		: ITextBox(rect, txt, id, pickable)
	{
	}

	CTextBox::~CTextBox()
	{
		_unregister(ui_ref);
	}

	void CTextBox::_register(IUserInterface* _ui)
	{
		if(ui_ref != NULL)
			_unregister(_ui);

		ui_ref = _ui;

		_ui->addToList(this);
	}

	void CTextBox::_unregister(IUserInterface* _ui)
	{
		ui_ref = NULL;

		_ui->removeFromList(this);
	}

	void CTextBox::setText(const char* txt)
	{
		text = txt;

		changed = true;
	}

	std::string CTextBox::getText()
	{
		return text;
	}

	Rect<unsigned int> CTextBox::getRect()
	{
		return rectangle;
	}

	void CTextBox::drawOut(char** table)
	{
		if(isClipped())
		{
			std::ostringstream s;
			s << "Error!\nElement with ID = ";
			s << id;
			s << " is out of UI! Please check the rectangle.";

			::MessageBoxA(NULL, s.str().c_str(), "ConGine", MB_OK | MB_ICONERROR);
			return;
		}

		int i = 0;

		for(int y = rectangle._UpperLeftCorner._y; y <= rectangle._LowerRightCorner._y; y++)
		{
			table[y][rectangle._UpperLeftCorner._x] = ' ';

			for(int x = rectangle._UpperLeftCorner._x + 1; x < rectangle._LowerRightCorner._x; x++)
			{
				if(i < text.size())
				{
					table[y][x] = text[i];
					i++;
				}
				else
					table[y][x] = ' ';
			}

			table[y][rectangle._LowerRightCorner._x] = ' ';
		}

		changed = false;
	}

	void CTextBox::makeActive(bool a)
	{

	}

	int CTextBox::getID()
	{
		return id;
	}

	bool CTextBox::isPickable()
	{
		return pickable;
	}

	bool CTextBox::hasChanged()
	{
		return changed;
	}

	bool CTextBox::isClipped()
	{
		if(!ui_ref)
			return true;

		unsigned int w = ui_ref->getWidth() - 1;
		unsigned int h = ui_ref->getHeight() - 1;

		if(rectangle._UpperLeftCorner._x < 0 || rectangle._LowerRightCorner._x > w ||
			rectangle._UpperLeftCorner._y < 0 || rectangle._LowerRightCorner._y > h)
			return true;

		return false;
	}
};