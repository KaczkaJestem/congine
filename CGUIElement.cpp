#include "CGUIElement.h"

namespace Engine
{
	CGUIElement::CGUIElement(Rect<unsigned int> _rectangle, int _id, bool _pickable)
		: IGUIElement(_rectangle, _id, _pickable)
	{
		
	}

	CGUIElement::~CGUIElement()
	{
		_unregister(ui_ref);
	}

	void CGUIElement::makeActive(bool a)
	{

	}

	int CGUIElement::getID()
	{
		return id;
	}

	bool CGUIElement::isPickable()
	{
		return pickable;
	}

	Rect<unsigned int> CGUIElement::getRect()
	{
		return rectangle;
	}

	void CGUIElement::_register(IUserInterface* _ui)
	{
		if(ui_ref != NULL)
			_unregister(_ui);

		ui_ref = _ui;

		_ui->addToList(this);
	}
	
	void CGUIElement::_unregister(IUserInterface* _ui)
	{
		ui_ref = NULL;

		_ui->removeFromList(this);
	}

	void CGUIElement::drawOut(char** table)
	{
		/*
			Writes the element straight to the viewport table.
			Overwrite this with children's virtual methods
		*/

		changed = false;
	}

	bool CGUIElement::hasChanged()
	{
		return changed;
	}

	bool CGUIElement::isClipped()
	{
		if(!ui_ref)
			return true;

		unsigned int w = ui_ref->getWidth() - 1;
		unsigned int h = ui_ref->getHeight() - 1;

		if(rectangle._UpperLeftCorner._x < 0 || rectangle._LowerRightCorner._x > w ||
			rectangle._UpperLeftCorner._y < 0 || rectangle._LowerRightCorner._y > h)
			return true;

		return false;
	}
};