#pragma once

#include <sstream>
#include "Rectangle.h"


namespace Engine
{
	template <class T> 
	class IValueBox abstract : public IGUIElement
	{
		protected:
			T value;

			virtual bool isClipped() = 0;

		public:
			IValueBox(Rect<unsigned int> _rect, T _value, int _id = -1, bool _pickable = false)
				: IGUIElement(_rect, _id, _pickable),
				  value(_value)
			{}

			~IValueBox() {}

			virtual void					makeActive(bool a) = 0;
			virtual void					setValue(T _value) = 0;

			virtual int						getID() = 0;
			virtual bool					isPickable() = 0;
			virtual Rect<unsigned int>		getRect() = 0;
			virtual T						getValue() = 0;
			virtual void					drawOut(char** table) = 0;
	};

};