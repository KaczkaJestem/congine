#pragma once

#include "IUserInterface.h"
#include "ITextBox.h"

namespace Engine
{
	class CTextBox : public ITextBox
	{
		protected:
			virtual bool isClipped();

		public:
			CTextBox(Rect<unsigned int> rect, std::string txt, int id = -1, bool pickable = false);
			~CTextBox();

			virtual void setText(const char* txt);
			
			virtual void					makeActive(bool a);
			virtual int						getID();
			virtual bool					isPickable();
			virtual bool					hasChanged();
			virtual std::string				getText();
			virtual Rect<unsigned int>		getRect();
			virtual void					drawOut(char** table);

			virtual void _register(IUserInterface* _ui);
			virtual void _unregister(IUserInterface* _ui);
	};
};