#include "Geometry.h"

namespace Engine
{
	Geometry* createGeometry(Pixel<unsigned int>* pixels, int pixelsCount)
	{
		Geometry* g = new Geometry;

		for(int i = 0; i < pixelsCount; i++)
			g->push_back(pixels[i]);

		return g;
	}
};

