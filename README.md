# ConGine

### Description
This is a simple Console Engine - a library written in C++ that provides a set of useful objects and methods to easily create your own graphical interface in the windows command line. It contains elements such as: buttons, text boxes, input boxes, viewports, progress bars and basic geometry.

The main.cpp file contains a demo of some  ConGine functionalities.

### Techonologies
* C++

### Building and using

If you want to use ConGine in your project in this stage of development, you should include the headers and sources directly in your project (look at the demo program in main.cpp).
