#pragma once

#include <windows.h>
#include <vector>
#include "Rectangle.h"


namespace Engine
{
	class IUserInterface;

	class IGUIElement abstract
	{
	protected:
			Rect<unsigned int>				rectangle;
			IUserInterface*					ui_ref;

			//	True if a change was made and it hasn't been rendered yet:
			bool changed;
			bool pickable;
			int id;

			virtual bool isClipped() = 0;

		public:
			IGUIElement(Rect<unsigned int> rect, int _id = -1, int _pickable = false)
				: rectangle(rect), 
				  ui_ref(NULL),
				  changed(false),
				  id(_id),
				  pickable(_pickable)
				{}

			~IGUIElement() {}

			virtual void				makeActive(bool a) = 0;

			virtual int					getID() = 0;
			virtual bool				isPickable() = 0;
			virtual bool				hasChanged() = 0;
			virtual Rect<unsigned int>	getRect() = 0;
			virtual void				drawOut(char** table) = 0;

			virtual void _register(IUserInterface* _ui) = 0;
			virtual void _unregister(IUserInterface* _ui) = 0;
	};

};