#pragma once

#include "IUserInterface.h"
#include "IButton.h"

namespace Engine
{
	class CButton : public IButton
	{
		protected:
			virtual bool isClipped();

		public:
			CButton(Rect<unsigned int> _rect, std::string _text, int _id, bool _pickable = true, bool _check = false);
			~CButton();

			virtual void setText(const char* txt);
			virtual void setID(int _id);
			virtual void setChecked(bool checked);

			virtual void					makeActive(bool a);

			virtual bool					isPickable();
			virtual int						getID();
			virtual bool					hasChanged();
			virtual Rect<unsigned int>		getRect();
			virtual std::string				getText();
			virtual bool					isChecked();
			virtual void					drawOut(char** table);

			virtual void _register(IUserInterface* _ui);
			virtual void _unregister(IUserInterface* _ui);
	};
};