#pragma once

#include <string>
#include "IGUIElement.h"


namespace Engine
{
	class IInputWindow : public IGUIElement
	{
		protected:
			std::string text;
			bool active;

			virtual bool isClipped() = 0;

		public:
			IInputWindow(Rect<unsigned int> _rect, std::string _text = "", int _id = -1, bool _pickable = true)
				: IGUIElement(_rect, _id, _pickable),
				  active(false)
			{
				text = _text;
			}

			~IInputWindow() {}

			virtual void				makeActive(bool a) = 0;
			virtual void				writeChar(char c) = 0;
			virtual void				setText(const char* txt) = 0;

			virtual std::string			getText() = 0;
			virtual int					getID() = 0;
			virtual bool				isPickable() = 0;
			virtual bool				hasChanged() = 0;
			virtual Rect<unsigned int>	getRect() = 0;
			virtual void				drawOut(char** table) = 0;

			virtual void _register(IUserInterface* _ui) = 0;
			virtual void _unregister(IUserInterface* _ui) = 0;
	};
};