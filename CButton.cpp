#include "CButton.h"


namespace Engine
{
	CButton::CButton(Rect<unsigned int> _rect, std::string _text, int _id, bool _pickable, bool _check)
	: IButton(_rect, _text, _id, _pickable, _check)
	{

	}
	
	CButton::~CButton()
	{
		_unregister(ui_ref);
	}

	void CButton::makeActive(bool a)
	{
		setChecked(a);
	}

	int CButton::getID()
	{
		return id;
	}

	bool CButton::isPickable()
	{
		return pickable;
	}

	void CButton::_register(IUserInterface* _ui)
	{
		if(ui_ref != NULL)
			_unregister(_ui);

		ui_ref = _ui;

		_ui->addToList(this);
	}
	
	void CButton::_unregister(IUserInterface* _ui)
	{
		ui_ref = NULL;

		_ui->removeFromList(this);
	}

	void CButton::setText(const char* txt)
	{
		text = txt;

		changed = true;
	}

	void CButton::setID(int _id)
	{
		id = _id;
	}

	void CButton::setChecked(bool _checked)
	{
		if(checked == _checked)
			return;

		checked = _checked;

		changed = true;
	}

	Rect<unsigned int>	CButton::getRect()
	{
		return rectangle;
	}
	
	std::string CButton::getText()
	{
		return text;
	}
	
	bool CButton::isChecked()
	{
		return checked;
	}

	void CButton::drawOut(char** table)
	{
		std::string _text = text;

		if(checked)
		{
			_text.insert(_text.begin(), '{');
			_text.insert(_text.end(), '}');
		}
		else
		{
			_text.insert(_text.begin(), '[');
			_text.insert(_text.end(), ']');
		}
		
		int i = 0;

		for(int y = rectangle._UpperLeftCorner._y; y <= rectangle._LowerRightCorner._y; y++)
		{
			table[y][rectangle._UpperLeftCorner._x] = ' ';

			for(int x = rectangle._UpperLeftCorner._x + 1; x < rectangle._LowerRightCorner._x; x++)
			{
				if(i < _text.size())
				{
					table[y][x] = _text[i];
					i++;
				}
				else
					table[y][x] = ' ';
			}

			table[y][rectangle._LowerRightCorner._x] = ' ';
		}

		changed = false;
	}

	bool CButton::hasChanged()
	{
		return changed;
	}

	bool CButton::isClipped()
	{
		if(!ui_ref)
			return true;

		unsigned int w = ui_ref->getWidth() - 1;
		unsigned int h = ui_ref->getHeight() - 1;

		if(rectangle._UpperLeftCorner._x < 0 || rectangle._LowerRightCorner._x > w ||
			rectangle._UpperLeftCorner._y < 0 || rectangle._LowerRightCorner._y > h)
			return true;

		return false;
	}
};