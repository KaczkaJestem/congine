#pragma once

#include "IUserInterface.h"

namespace Engine
{

class IEventReceiver abstract
{
	protected:
		IUserInterface* ui;

	public:
		virtual void setUserInterface(IUserInterface* _ui) = 0;
		virtual bool isKeyPressed(int keyCode) = 0;
		virtual void update() = 0;
};

};