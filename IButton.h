#pragma once

#include <string>
#include "IGUIElement.h"
#include "Rectangle.h"

namespace Engine
{
	class IButton abstract : public IGUIElement
	{
		protected:
			std::string text;

			int id;
			bool checked;

			virtual bool isClipped() = 0;

		public:
			IButton(Rect<unsigned int> _rect, std::string _text, int _id = -1, bool _pickable = true, bool _check = false)
				: IGUIElement(_rect, _id, _pickable),
				  text(_text),
				  id(_id),
				  checked(_check)
				{}

			~IButton() {}

			virtual int	 getID() = 0;
			virtual bool isPickable() = 0;
			virtual void makeActive(bool a) = 0;
			virtual void setText(const char* txt) = 0;
			virtual void setID(int) = 0;
			virtual void setChecked(bool checked) = 0;

			virtual Rect<unsigned int>		getRect() = 0;
			virtual std::string				getText() = 0;
			virtual bool					isChecked() = 0;
			virtual void					drawOut(char** table) = 0;
	};

};