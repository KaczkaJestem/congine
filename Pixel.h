#pragma once

#include <vector>
#include "Position2d.h"


namespace Engine
{

template<class T>
class Pixel
{
public:

	Pixel(Position2d<T> position, char ch)
		: _position(position),
		  _char(ch)
	{
	}

	Position2d<T> _position;
	char _char;
};

};